from flask import jsonify, request, Flask, render_template
import json
from predict import predict
import os


app = Flask(__name__)

app._static_folder = os.path.abspath("templates/static/")

@app.route("/predict", methods=["POST","GET"])
def predict_by_picture():
    payload = request.data.decode("utf-8")
    picture = json.loads(payload)["picture"]
    result = predict(picture)
    print(result)
    return json.dumps({"feeling":result})

if __name__=="__main__":
    app.run()