import tensorflow as tf
from tensorflow import keras
from PIL import Image
import json
import numpy as np

model = keras.models.load_model("recognition.v5")


def reload_model():
    global model
    model = keras.models.load_model("recognition.v5")
rgb_to_range = lambda rgb_code: 1 - ((rgb_code[0] + rgb_code[1] + rgb_code[2]) / (3 * 255))

def predict(current_image:list):
    unknown_image = np.random.normal(0, 1, (1, 25, 25))

    #current_image = json.loads(picture)
    for x in range(25):
        for y in range(25):
            unknown_image[0][x][y] = current_image[x][y]
    unknown_image = tf.constant(unknown_image, dtype=tf.int32)

    prediction = model.predict(unknown_image)[0]
    return ["fröhlich","traurig","neutral","staunend"][list(prediction).index(np.max(prediction))]


if __name__=="__main__":
    unknown_image = np.random.normal(0,1, (25,25, 25))

    for i in range(25):
        current_image = Image.open(f"dataset/neutral/{150 + i}.png").load()
        for x in range(25):
            for y in range(25):
                unknown_image[i][x][y] = rgb_to_range(current_image[x, y])

    unknown_image = tf.constant(unknown_image, dtype=tf.int32)

    prediction = model.predict(unknown_image)
    k = 0
    for a in prediction:
        print(list(a).index(np.max(a)))


