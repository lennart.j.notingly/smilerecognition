import tensorflow as tf
from tensorflow import keras
import random
from PIL import Image
import numpy as np

model = keras.Sequential([
    keras.layers.Flatten(input_shape=(25,25)),
    keras.layers.Dense(56,activation='relu'),
    keras.layers.Dense(4,activation='softmax'),
])

model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy']
)

pics = np.random.normal(0,1, (255*3+200, 25, 25))
labls = np.random.randint(0, 2, 255*3+200)


rgb_to_range = lambda rgb_code: 1-((rgb_code[0]+rgb_code[1]+rgb_code[2])/(3*255))
for i in range(1,250):
    current_image = Image.open(f"dataset/happy/{i}.png").load()
    for x in range(25):
        for y in range(25):
            pics[i][x][y] = rgb_to_range(current_image[x, y])
    labls[i] = 0
for i in range(1, 250):
    current_image = Image.open(f"dataset/sad/{i}.png").load()
    for x in range(25):
        for y in range(25):
            pics[250+i][x][y] = rgb_to_range(current_image[x, y])
    labls[250+i] = 1

for i in range(1, 250):
    current_image = Image.open(f"dataset/neutral/{i}.png").load()
    for x in range(25):
        for y in range(25):
            pics[250+250+i][x][y] = rgb_to_range(current_image[x, y])
    labls[250+250+i] = 2

for i in range(1, 200):
    current_image = Image.open(f"dataset/stunning/{i}.png").load()
    for x in range(25):
        for y in range(25):
            pics[250+250+250+i][x][y] = rgb_to_range(current_image[x, y])
    labls[250+250+250+i] = 3

permutation = np.random.permutation(len(labls))
labls = labls[permutation]
pics = pics[permutation]


images = tf.constant(pics, dtype=tf.float32)  # X is a np.array
labels = tf.constant(labls, dtype=tf.int32)

model.fit(images,labels,epochs=12)


model.save("recognition.v5")

